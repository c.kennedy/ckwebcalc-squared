<?php
class SquaredNegativeException extends Exception {};
class SquaredZeroException extends Exception {};
class NonIntegerException extends Exception {};

function squared($x)
{
    
    try
    {
        if ($x == 0)
        {
            throw new SquaredZeroException();
        }
        else if ($x < 0)
        {
            throw new SquaredNegativeException();
        }
        else if ($x !== is_int)
        {
            throw new NonIntegerException();
        }
    }
    catch (SquaredZeroException $ex)
    {
        echo "Square of zero exception!";
    }
    catch (SquaredNegativeException $ex)
    {
        echo "Squared by negative number exception!";
    }
    catch (NonIntegerException $ex)
    {
        echo "You can only use the square function with whole Integers!";
    }
    catch (Exception $x)
    {
        echo "UNKNOWN EXCEPTION!";
    }
    
    
    $answer = x * x;
    return $answer;
}