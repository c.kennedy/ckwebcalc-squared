<?php
echo "Test Script Starting\n";
require('functions.inc.php');

$x=10;
$expect=100;

$answer=squared($x);

echo "Test Result: ".$x."² =".$answer." (expected: ".$expect.")\n";

if ($answer==$expect)
{
    echo "Test Passed\n";
    exit(0); 
}
else
{
    echo "Test Failed\n";
    exit(1); 
}
