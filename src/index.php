<?php
header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
require('functions.inc.php');

$output = array(
	"error" => false,
	"string" => "",
	"answer" => 0
);

$x = $_REQUEST['x'];

$answer=squared($x);

$output['string']="²".$x."=".$answer;
$output['answer']=$answer;

echo json_encode($output);
exit();
